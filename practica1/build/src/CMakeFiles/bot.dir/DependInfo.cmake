# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/victor/sii-50149/practica1/src/Esfera.cpp" "/home/victor/sii-50149/practica1/build/src/CMakeFiles/bot.dir/Esfera.cpp.o"
  "/home/victor/sii-50149/practica1/src/MundoCliente.cpp" "/home/victor/sii-50149/practica1/build/src/CMakeFiles/bot.dir/MundoCliente.cpp.o"
  "/home/victor/sii-50149/practica1/src/MundoServidor.cpp" "/home/victor/sii-50149/practica1/build/src/CMakeFiles/bot.dir/MundoServidor.cpp.o"
  "/home/victor/sii-50149/practica1/src/Plano.cpp" "/home/victor/sii-50149/practica1/build/src/CMakeFiles/bot.dir/Plano.cpp.o"
  "/home/victor/sii-50149/practica1/src/Raqueta.cpp" "/home/victor/sii-50149/practica1/build/src/CMakeFiles/bot.dir/Raqueta.cpp.o"
  "/home/victor/sii-50149/practica1/src/Vector2D.cpp" "/home/victor/sii-50149/practica1/build/src/CMakeFiles/bot.dir/Vector2D.cpp.o"
  "/home/victor/sii-50149/practica1/src/bot.cpp" "/home/victor/sii-50149/practica1/build/src/CMakeFiles/bot.dir/bot.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "../include"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
