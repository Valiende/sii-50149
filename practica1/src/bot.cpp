#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>

#include "DatosMemCompartida.h"


int main(){

	DatosMemCompartida *Memoria;
	int fichbot; 	
	
	//printf("Hola\n");

	fichbot = open("/tmp/Bot", O_RDWR);
	Memoria = (DatosMemCompartida*) mmap(NULL, sizeof(DatosMemCompartida), PROT_READ|PROT_WRITE, MAP_SHARED, fichbot, 0);
	//mmap(NULL, length, PROT_READ, MAP_SHARED, fichbot, 0);
	close(fichbot);

	//printf("Que tal\n");
	//printf("%ld\n", Memoria);

	while(1){
		if (Memoria->accion1 == 2) break;

		if (Memoria->raqueta1.y1 < Memoria->esfera.centro.y) Memoria->accion1=1;
		else if (Memoria->raqueta1.y2 > Memoria->esfera.centro.y) Memoria->accion1=-1;
		else Memoria->accion1=0;

		if (Memoria->inactivo >= 400){
			if (Memoria->raqueta2.y1 < Memoria->esfera.centro.y) Memoria->accion2=1;
			else if (Memoria->raqueta2.y2 > Memoria->esfera.centro.y) Memoria->accion2=-1;
			else Memoria->accion2=0;}

		//Memoria->accion=1;
		//printf("Estas\n");		
		
		usleep(25000);
	
	}

	printf("Fin del programa\n");

	munmap(Memoria, sizeof(DatosMemCompartida));

	return 0;
}
